Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0019-laser-head-60w-dv).

|document|download options|
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s2-0019-laser-head-60w-dv/-/raw/main/01_operating_manual/S2-0019_B_BA_Laser%20Head.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s2-0019-laser-head-60w-dv/-/raw/main/02_assembly_drawing/s2-0019_A_ZNB_laser_head_60w_dv.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s2-0019-laser-head-60w-dv/-/raw/main/03_circuit_diagram/S2-0019-EPLAN-B.pdf)|
|maintenance instructions   |[de](https://gitlab.com/ourplant.net/products/s2-0019-laser-head-60w-dv/-/raw/main/04_maintenance_instructions/S2-0019_A_Wartungsanweisungen.pdf)|
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s2-0019-laser-head-60w-dv/-/raw/main/05_spare_parts/S2-0019_A_EVL.pdf); [en](https://gitlab.com/ourplant.net/products/s2-0019-laser-head-60w-dv/-/raw/main/05_spare_parts/S2-0019_B_EVL%20engl.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
