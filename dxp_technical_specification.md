The S2-0019 LASER HEAD 60W DV has the following technical specifications.

## Technical information
| Gewicht                             | 10.5 kg                                             |
| :---------------------------------- | --------------------------------------------------- |
| Größe                               | 79 × 447 × 580 mm                                   |
| Verfahrbereich in Z in mm           | 150                                                 |
| Laserart                            | Diodenlaser mit Dauerstrich                         |
| Wellenlänge in nm                   | 974                                                 |
| Leistung in W                       | 60                                                  |
| Arbeitsabstand (Linsen-Fokus) in mm | 100                                                 |
| Min. Laserspot in mm (Ø)            | 0,2                                                 |
| Pyrometer                           | Einfarbenpyrometer; 10.000 Temperaturmessungen/Sek. |
| Spannung in V                       | 24                                                  |
| Max. Stromstärke in A               | 8                                                   |
| Kommunikationsschnittstelle         | 2x Ethernet, UNICAN                                 |